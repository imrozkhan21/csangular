import { NgModule } from '@angular/core';

import {

  MatAutocompleteModule,

  MatButtonModule,

  MatCardModule,

  MatCheckboxModule,

  MatDialogModule,

  MatFormFieldModule,

  MatIconModule,

  MatInputModule,

  MatMenuModule,

  MatPaginatorModule,

  MatRadioModule,

  MatSelectModule,

  MatSidenavModule,

  MatSnackBarModule,

  MatTooltipModule,

  MatGridListModule,

  MatListModule,

  MatSlideToggleModule,

  MatProgressSpinnerModule,

  MatSliderModule

} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';



@NgModule({

  exports: [

    MatAutocompleteModule,

    MatButtonModule,

    MatCardModule,

    MatCheckboxModule,

    MatDialogModule,

    MatFormFieldModule,

    MatIconModule,

    MatInputModule,

    MatMenuModule,

    MatPaginatorModule,

    MatRadioModule,

    MatSelectModule,

    MatSidenavModule,

    MatSnackBarModule,

    MatTooltipModule,

    MatToolbarModule,

    MatGridListModule,

    MatListModule,

    MatSlideToggleModule,

    MatProgressSpinnerModule,

    MatSliderModule

  ]

})



export class MaterialModule { }

