import { Injectable } from '@angular/core';
import {LEADERS} from "../shared/leaders";
import {Leader} from "../shared/leader";

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';

@Injectable()
export class LeaderService {

  constructor() { }

  getCorporateLeaders() : Observable<Leader[]>{
  return Observable.of(LEADERS);
  }

  getFeaturedLeader(): Observable<Leader> {
    return Observable.of(LEADERS.filter((leader) => leader.featured)[0]);
  }

}
