import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module'
import { FlexLayoutModule } from '@angular/flex-layout';

import {MenuComponent} from "./menu/menu.component";
import {DishDetailComponent} from "./dish-detail/dish-detail.component";
import { AppComponent } from './app.component';
import {FooterComponent} from "./footer/footer.component";
import {HeaderComponent} from "./header/header.component";
import {AboutComponent} from "./about/about.component";
import {ContactComponent} from "./contact/contact.component";
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";

import { DishService } from './services/dish.service';
import {PromotionService} from "./services/promotion.service";
import {LeaderService} from "./services/leader.service";

import { AppRoutingModule } from './app-routing/app-routing.module';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { baseURL } from './shared/baseurl';

import { ProcessHTTPMsgService } from "./services/process-httpmsg.service";

import {HighlightDirective} from "./directives/highlight.directive";


import 'hammerjs'


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DishDetailComponent,
    FooterComponent,
    HeaderComponent,
    AboutComponent,
    ContactComponent,
    HomeComponent,
    LoginComponent,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [DishService, PromotionService, LeaderService,  {provide: 'BaseURL', useValue: baseURL}, ProcessHTTPMsgService],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent
  ]
})
export class AppModule { }
